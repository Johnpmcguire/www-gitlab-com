---
layout: handbook-page-toc
title: "PathFactory"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Uses  
PathFactory (PF) is leveraged as our content library & content distribution system. The [Digital Marketing team](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/) is primarily responsible for all the of the content within the PathFactory tool. To request the creation of a track or need to leverage PathFactory in a marketing intiative, please see the [instructions provided by the Digital Marketing team](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/pathfactory-management/#using-pathfactory). 


### Access   
The Marketing Operations team is responsible for managing access requests & provisioning the appropriate level of access for each role/function. Providing access is generally hanlded via baseline entitlement; however, if you need access and it is *not* part of your roles baseline entitlement please open a [`Single Person Access Request` issue](/handbook/business-ops/it-ops-team/access-requests/#single-person-access-request) and provide business reason why access is needed.   

#### User Roles   
There are three levels of access - Admin, Author, Reporter - which are granted based on the persons' role and team function within GitLab. **All access levels** have the ability to view the analtyics page within the tool. 

##### Admin  
Admin access is primarily granted to Marketing Operations only; for PathFactory an `Admin` seat has been granted to the **Digital Marketing Program Manager** because this role is reponsible for creating, curating and managing all tags/tracks within PathFactory.  

##### Author 
`Author` access allows user to build, edit and publish content tracks applying existing tags to the assets. This level of access is granted to the Marketing Program Managers and select Content team members.  

##### Reporter 
`Reporter` access provides general visibility to content within PathFactory but does not allow end user to create or modify any of the content, tracks or tags. This level of access is granted for the general GitLab team member both within Marketing and elsewhere who have a business need to have access.  


### Listening Campaigns 
In Marketo, we have programs built to "listen" for PathFactory (PF) activity & content consumption allowing us to track behaviour without having to physically gate all the assets which disrupts the user experience.   

The PF<>Marketo listening programs are built to triggered based on the `slug` associated to each of asset. **Very Important: Do NOT to change the `slug` in PF without notifying Ops *prior* to making the change**. Each listening campaign has a Salesforce (SFDC) campaign associated to it tracking consumption and applying Bizible touchpoints.  

The naming convention for each of the listeners is specific to the asset type & is used as a trigger to the appropriate scoring campaign *within Marketo* at this time these listening campaigns have **no impact** on PathFactory engagement scores. The same naming convention is used for **both** Marketo & Salesforce campaigns.   

| Asset Type | Naming Convention |
| :--------- | :---------------- |
| Whitepaper | PF - Whitepaper - | 
| eBook      | PF - eBook -      | 
| Analyst Report | PF - Analyst Report - | 
| Research Report | PF - Research Report - | 
| Demo  | PF - Demo - | 
| Datasheet | PF - Datasheet - |   

#### Salesforce Campaign Type  

For the PathFactory listening campaigns there is a corresponding Salesforce `Campaign Type` to be used. The `Campaign Member Status` simply tracks if the content was consumed. This Salesforce `Campaign Type` should be used for **nothing** else. For greater details, see the [Business Ops Resources - Campaign details](/handbook/business-ops/resources/#campaign-type--progression-status).

### PathFactory SFDC Panel   

In Salesforce (SFDC) there is a `PathFactory` section on both the LEAD and CONTACT layout that provides information about Pathfactory content consumption. 

1. These fields are all **session** based - this is by design from PathFactory and the field values overwrite with new data for every session. 
1. A piece of content is considered "consumed", by default, when it has been viewed for 20 seconds minimum. 
     - This can be modified per asset. 
     - At this time all assets have been set up the same. If that changes, this page will be updated with details.
1. A PF "session" is a single content consumption period. 
1. A "session" closes after 30 minutes of inactivity or at 11:59pm (instance set timezone - SFDC is set to Pacific time).
1. If a person consumes more than one content track in a **single session** all of the assets/tracks viewed will be displayed in the fields cumulatively. 

#### PF Panel Fields

![](/images/handbook/marketing/marketing-operations/pf_sfdcpanel.png)

| Field Name | Purpose | 
|:---------- | :------ | 
| PathFactory Experience Name | The PathFactory track name - [more details](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/pathfactory-management/#content-track-management) | 
| PathFactory Assets Viewed | Cumulative number of assets viewed. **This is not associated to time consumed! See Content Count for difference** |
| PathFactory Asset Type | A tag to help categorize types of content (whitepaper, video, eBook, etc) |
| PathFactory Funnel State | Each asset is tagged with stage of funnel most applicable to asset - Top of Funnel, Middle of Funnel or Bottom of Funnel | 
| PathFactory External ID | A non-unique ID that can be added to tracks &/or assets, which can be leveraged to organize content and configure it in Marketing Automation Platform (i.e Marketo) | 
| PathFactory Engagement Score | Each asset in content library can be assigned an engagement score; this score is passed from PF to SFDC and used to determine meaningful engagement with content. | 
| PathFactory Engagement Time | Cumulative time a person spends consuming assets in session. | 
| PathFactory Content Count | Cumulative count of assets consumed. Example: if person consumes 2 whitepapers, 1 video and blog post for *minimum of 20 seconds each* in a **single session** this field would show 4. | 
| PathFactory Content List | Cumulative list of each assets content id/slug for each asset consumed. | 
| PathFactory Topic List | Assets are tagged by **topic**. This is manually set & aligns with the [tracking content](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/pathfactory-management/#tracking-content) list. | 