---
layout: markdown_page
title: "Category Direction - Epics"
---

- TOC
{:toc}

## Description

Large enterprises are continually working on increasingly more complex and larger
scope initiatives that cut across multiple teams and even departments, spanning months,
quarters, and even years. GitLab's vision is to allow organizing these initiatives
into powerful **multi-level work breakdown** plans and enable **tracking the execution**
of them over time, to be extremely simple and insightful. In addition, GitLab should
highlight which **opportunities have higher ROI**, helping teams make strategic
business planning decisions.

GitLab's **multi-level work breakdown** planning capabilities will include [multiple layers of epics](https://gitlab.com/groups/gitlab-org/-/epics/312)
and [issues](https://gitlab.com/groups/gitlab-org/-/epics/316), allowing enterprises to capture:

- High-level strategic initiatives and OKRs (objectives and key results).
- Portfolios, programs, and projects.
- Product features and other improvements.
- [Work roll ups](https://gitlab.com/groups/gitlab-org/-/epics/312) and [dependency management](https://gitlab.com/groups/gitlab-org/-/epics/316).

GitLab will help teams **identify high ROI opportunities**, by organizing and surfacing
initiatives with relatively [large](https://gitlab.com/gitlab-org/gitlab-ee/issues/7718)
[benefits](https://gitlab.com/gitlab-org/gitlab-ee/issues/8115) (as entered by team members)
and relatively low costs (via rolled up effort estimates at the epic level).

GitLab supports [enterprise Agile portfolio and project management frameworks](https://about.gitlab.com/solutions/agile-delivery/),
including Scaled Agile Framework (SAFe), Scrum, and Kanban.

[See how GitLab team-members themselves use GitLab Agile PPM to create GitLab, as
of GitLab 11.4.](https://www.youtube.com/watch?v=ME9VwseXMuo)

See [upcoming planned improvements for portfolio and project management](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=EnterpriseAgile).

## What's next & why

We've written a [mock press release](/direction/plan/portfolio_management/one_year_plan_portfolio_mgmt) describing where we intend to be by 2020-09-01. We will maintain this and update it as we sense and respond to our customers and the wider community.

We now have the basic epic object working in GitLab. You can attach issues to epics and you can view epics in a roadmap visualization. The next step is to introduce more flexibility in epic relationship structures, to address more use cases. In particular, the next step is building out flexible work breakdown structures with epics of epics:

- [Surfacing Epic Relationships in Roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/312)
- [Show related Epics within Parent Epic](https://gitlab.com/groups/gitlab-org/-/epics/644)
- [Epics inherit Start/End Dates from their Sub-Epics](https://gitlab.com/gitlab-org/gitlab/issues/7332)

## Competitive landscape

Portfolio Management via Epics is not a radically new category, and there are established players such as Clarity, Planview, VersionOne, AgileCraft, and ServiceNow. Many of these older tools were developed targeted at truly enterprise cases, allowing users to track large business initiatives across an organization. Customers using these tools typically have another set of tools for the product-development teams to turn these high-level business initiatives into scoped out detailed planned work and actual software deliverables. Therefore, our competitive advantage is having _both_ (the high-level initiatives, and the product-development-level abstractions) in a single tool, that is fully integrated for a seamless experience. Our strategy is building _toward_ those enterprise use cases and abstractions, starting with the product-development baseline abstractions. And so we are developing multi-level child epics, and adding functionality there incrementally.

## Analyst landscape

We are working with analysts to better understand the space of Portfolio Management as it relates to work breakdown structures. For example, Gartner has an area called [Enteprise Agile Planning Tools](https://www.gartner.com/reviews/market/enterprise-agile-planning-tools/vendors), which this GitLab category is very much a part of.

Analysts in this space are concerned with truly "enterprise" use cases. Agile methodologies have been around for a good number of years, but they have been focused on small teams, or maybe a small number of teams working closely in concert to execute and deliver features. The industry is now focused on how to take these processes that have been proven in small teams, and scale them to large enterprises with business initiatives that span potentially even multiple departments. What is crucial therefore, is how can organizations deliver business value very quickly, but still allow stakeholders (especially executive stakeholders) the visibility to track progress and be assured that the enterprise is working on the right initiatives in the first place. This has even led to the popular [SAFe (Scaled Agile Framework) methodology](https://www.scaledagileframework.com) as a way to help guide enterprise organizations in their Agile transformations.

At GitLab, we embrace this enterprise Agile transformation that companies are going through and want to lead the way by building the tools to do so. In particular, having a single application to view your entire work breakdown structure, from executive level, all the way down to issues and merge requests and even commits, means that our design truly allows for streamlining that visibility. We are focused on building this work breakdown structure out as the initial goal in Agile Portfolio Management.

## Top Customer Success/Sales issue(s)

The top feature in this area is indeed [flexible work breakdown structures](https://gitlab.com/groups/gitlab-org/-/epics/312). We are working towards this by advancing providing [Epic to Epic Relationships](https://gitlab.com/groups/gitlab-org/-/epics/644) and [Roadmap-based Visualizations](https://gitlab.com/groups/gitlab-org/-/epics/312).

## Top User Issues

Many users are interested in harmonizing epics and issues together, to enable stronger top down, and bottom up planning.

In particular, we are focusing on:

- [Epics at the Project Level](https://gitlab.com/gitlab-org/gitlab/issues/31840)
- [More Acessible and Discoverable Epics](https://gitlab.com/gitlab-org/gitlab/issues/12785)
- [Filter Issue lists by Epic](https://gitlab.com/gitlab-org/gitlab/issues/9029)

## Top Vision Item(s)

**Increase Functionality of Epics :**

- [Enable Confidential Epics](https://gitlab.com/gitlab-org/gitlab/issues/197339)
- [Single Level Group Epics](https://gitlab.com/gitlab-org/gitlab/issues/37081)
- [Understand Health/Risk Status of Work](https://gitlab.com/gitlab-org/gitlab/issues/36427)
