---
layout: markdown_page
title: Product Section Direction - Secure
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Proactively identify vulnerabilities and weaknesses to minimize risk</b>
    </font>
</p>

GitLab Secure enables accurate, automated, and continuous assessment of your applications and services enabling you to proactively identify vulnerabilities and weaknesses to minimize your security risk.  Secure is not an additional step in your process nor an additional tool to introduce to your process. it is woven into your DevOps cycle thus allowing you to adapt your security testing and processes to your developers (and not the other way around).

![Secure Overview](/images/direction/secure/secure-overview.png)

## Section Overview

The Secure Section focuses on identifying security findings (e.g., vulnerabilities and weaknesses) within applications and services prior to moving them to operations. Furthermore, Secure can (and will) provide security visibility for applications and services already deployed to production. Secure’s goal is to proactively identify vulnerabilities and weaknesses before they are exploited.  This is done by:

* Applying a proactive approach to security with a focus on identifying security findings
* Analyzing applications and services including cloud-native infrastructure for security vulnerabilities and weaknesses
* Leveraging common methods include ethical hacking and fuzz testing as well as static and dynamic analysis

### Groups

The Secure Section is made up of one DevOps stage, Secure, and seven groups supporting the major categories of DevSecOps including:

* Static Analysis - Assess your applications and services by scanning your source code for vulnerabilities and weaknesses.
* Dynamic Analysis - Assess your applications and services while they are running by leveraging the Review App available as part of GitLab’s CI/CD functionality.
* Composition Analysis - Assess your applications and services by analyzing dependencies for vulnerabilities and weaknesses, confirming only approved licenses are in use, and scanning your containers for vulnerabilities and weaknesses.
* Attack Surface - Assess your applications, services, and cloud-native infrastructure in operations to verify they are not vulnerable to the latest security threats.
* Exposure Analysis - Assess your applications, services, and cloud-native infrastructure for vulnerabilities and weak configurations (e.g., missing authentication on cloud storage).
* Vulnerability Research - Leverage GitLab research to empower your Secure results by connecting security findings to industry references like [CVE IDs](https://cve.mitre.org).

### Resourcing and Investment

The existing team members for the Secure Section can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=secure-section)
* [User Experience](https://about.gitlab.com/company/team/?department=secure-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=secure-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=secure-enablement-qe-team)


<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/caZXzNCgI9k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## SWOT Analysis & Challenges

### Strengths

* Complete DevOps platform with seamless connections between SCM, CI/CD, Secure, Ops, and Defend.
* Enable “Shift Left” testing paradigm as Secure is directly integrated into the developer workflow.
* Ability to begin leveraging DevSecOps with GitLab requires just a few lines within the user’s .gitlab-ci.yml configuration.
* Exceptionally talented product team with deep industry expertise.
* Expansive 1-year plan and 3-year strategy which is hyper-focused on market needs (both currently needed and expected based on trends).
* Investments being made in both development and operations testing providing GitLab users visibility across the entire DevSecOps lifecycle.

### Weaknesses

* Our SaaS product (GitLab.com) is not yet enterprise grade, and several improvements are needed for enterprise adoption of self-managed instances.
* Many Secure categories are addressed today through the usage of open source software (OSS) security tools which are not as robust as their commercial counterparts.
* Several Secure categories require more than the time allotted to CI/CD pipeline tasks to fully scan and assess code commits and asynchronous pipelines are not yet available.
* The Secure Section categories are not backed by vulnerability researchers focused on identifying zero-day or near zero-day vulnerabilities which could be leveraged as part of our customer’s Secure experience.
* Our company name may be limiting our opportunities for the Secure Section as it implies we are focused on lab or non-operations testing as well as only the developer within the software development lifecycle.
* Secure only being available within the Ultimate (self-hosted) and Gold (GitLab.com) may lead to less adoption due to the price point.

### Opportunities

* Consideration of new business models/licensing plans could lead to additional adoption of features.
* Take a leadership position within Application Security Testing (AST) market addressing Static Analysis, Dynamic Analysis, Interactive Analysis, Fuzz Testing, and Dependency Scanning.
* Take a leadership position within adjacent markets including Vulnerability Assessment (VA) as well as Breach and Attack Simulation (BAS) allowing security teams to stay within a single platform context.
* Provide complete DevSecOps platform allowing security teams to manage all vulnerabilities across development and operations with a focus on lowering security risk.
* Provide first-class integrations allowing users to bring their own tools while still staying within the single platform context that GitLab provides.
* Big opportunity to grow and shift usage to SaaS, allowing users to receive value more quickly from GitLab without provisioning and maintaining their own instances.
* Drive internal adoption of the features we are shipping. We have a large development team and we should aspire to have them dogfood new features such that we practice good DevSecOps processes.

### Threats

* We have entered the Application Security Testing (AST) market late forcing us to play catchup with competing offerings (both single purpose security tools and all-in-one DevOps solutions).
* Primary competitors within the DevOps space (e.g., Microsoft / GitHub) are in a position to acquire their way into the DevSecOps space putting pressure on us to differentiate.
* Security issues within other Sections that are not addressed as quickly as possible will lead to a lowered opinion of our platform causing lack of trust in our security credibility as well as impact retention and IACV.
* Heavily relying on open source software (OSS) security tools could lead to a lack of differentiation as others can leverage the same tools within their pipelines.
* Onboarding new team members within the Secure Section has been slow leading to lower velocity than desired leading to delays in new feature delivery.
* If we stop talking to customers to better understand their problems and pain points, we will continue to ship code and features at a high velocity, but the things we ship may not add value for our customers. This is known as the [build trap](https://melissaperri.com/blog/2014/08/05/the-build-trap).

## Themes

### Security Is A Team Effort

Everyone benefits when security is a team effort as testing happens regularly, issues are found earlier, and code shipped is more secure.  To make this possible, security must be approachable, not overburden teams, and results must be easy to interpret.  Security testing tools and processes must be [adapted to your developers](https://www.gartner.com/document/code/450792?ref=ddisp&refval=450792) (and not the other way around).  This means bringing security into the workflow of your developers such that they can stay within their context without having unnecessary steps added to their daily work.  Furthermore, results provided as security findings must be presented in a way that they can be interpreted without needing a PhD in cybersecurity.  This includes providing enough detail to begin identifying the root cause (including identifying the section of code causing the security finding) and suggesting remediation steps (including auto-remediation) as well as pointing to industry standards related to the security finding.  By implementing an integrated DevSecOps lifecycle with actionable results, security becomes everyone’s responsibility.

As examples, GitLab will provide:
* Security findings within the merge request view allowing developers to stay within their context.
* Security gates enabling the ability to require approval to merge when security findings are above a pre-set severity.
* Industry references with security findings to aid in root cause analysis and remediation.
* Auto-remediation, where applicable, enabling faster resolution of security findings.
* Summary views, or dashboards, showing the overall health of the project, group, and instance enabling security teams to contribute and interact with development teams.
* Historical reporting for security findings such that root cause can be identified and addressed, preventing recurring issues.

### Shift Left. No, More Left Than That.

The “shift left” approach is not a new concept within software testing and DevOps best practices.  It is commonly thought of when discussing the [DevSecOps lifecycle](https://www.devsecops.org/blog/2016/5/20/-security). This usually includes security testing earlier in the software development lifecycle with the goal of identifying security vulnerabilities and weaknesses prior to shipping code to operations.  Today’s techniques include static application security testing (SAST), dynamic application security testing (DAST), interactive application security testing (IAST), dependency scanning, and license compliance.  The continuation of the “shift left” approach requires a harder shift left, bringing security testing as close as possible to the developer.  This enables earlier detection of security vulnerabilities and weaknesses, thus lowering the cost of remediation (as well as reducing work for the entire team as security findings are addressed prior to reaching the QA and security teams).

As examples, GitLab will provide:
* Static analysis beyond traditional SAST functionality, enabling more ways to [white-box test](https://en.wikipedia.org/wiki/White-box_testing) source code.
* Expanded identification functionality, enabling secret detection beyond API tokens, passwords, and cryptographic keys with improved secrets management leveraging cloud-native technologies.
* Real-time feedback to developers via integrated development environments (IDEs), enabling developers to write secure code prior to code commit.

### Shift Right. Yes, Right. Right Into Operations.

Security testing doesn’t stop once code is shipped.  New vulnerabilities, security weaknesses and, attacker techniques are constantly discovered, leaving operations and their associated applications and services open to being compromised.  Also, as organizations continue to shift to the cloud and employ cloud-native strategies, new attack surfaces are exposed that did not exist within the traditional data center.  These include items like cloud storage permissions and unwanted network services. Applications, services, and their associated cloud-native infrastructure need to be assessed just as a user (or an attacker) would interact with them.  This means performing the same tasks that an attacker would perform including reconnaissance, vulnerability assessment, and penetration testing.  Implementing a continuous assessment strategy of operations is needed to provide full visibility into all potential risk.

As examples, GitLab will provide:
* Dynamic analysis categories, including DAST and DAST API, providing the ability to assess the operations side of the DevOps lifecycle.
* New categories purposefully designed to assess the operations network including automatic penetration testing and vulnerability assessment functionality.
* Cloud-native assessment capabilities allowing users to truly understand their attack surface.
* Active monitoring of public key infrastructure (PKI), providing full visibility into the cryptographic health of your cloud-native environment (and associated containers).

### Provide Active Intelligence to Enable Data-Driven Decisions

Security findings, without context, can lead to making incorrect decisions on remediation, leaving applications and services vulnerable. With more data made available, better decisions can be made while prioritizing security findings, enabling users to best manage their security risk.  Furthermore, this enables developers to write secure code, build operations to package dependencies free of vulnerabilities, and security team members to test deeper than previously achievable.  Leveraging machine learning provides active intelligence, enabling users to make smart, data-driven decisions at the right time, lowering overall security risk.  Machine learning, to be successful, relies on big data to build accurate models. The GitLab community includes developers, build operators, and security teams all working together within their organizations to code, build, deliver and secure application and services going into operations.  As a global community, developers can learn from each other to identify and better secure code. Dependency vulnerabilities and weaknesses can be avoided, and security teams can test smarter, faster, and deeper.  Machine learning, powered by anonymized data, provides active intelligence enabling data-driven decisions.

As examples, GitLab will provide:
* Machine learning that will be opt-in when it relates to using anonymized user data from the GitLab community.
* Actionable intelligence to identify insecure coding practices to help developers write more secure code in real-time.
* Dynamic application security testing (DAST) tailored to the application or service being tested, identifying security findings which may have been missed with traditional testing techniques.
* Real-time data about software vulnerabilities and weaknesses related to technologies used within users’ projects, groups, and instances.
* Auto-remediation of discovered software vulnerabilities and weaknesses within project source code, dependencies (e.g., libraries and packages), and containers.
* Autogeneration of signatures and policies to address security findings within Secure Section categories that can be automatically applied to Defend Section categories.

### BYOT - Bring Your Own (Security) Tools

At GitLab, we believe everyone can contribute and security is everyone’s responsibility.  We empower GitLab users by providing security tools which include Static Analysis, Dynamic Analysis, Container Scanning, Dependency Scanning, and License Scanning.  However, we recognize our users may have existing security tools and may want to continue to use them.  As such, we strive to play well with others.  Security tool vendors will be provided standardized components (e.g., APIs) within the GitLab complete DevOps platform enabling easy integration into our Security Dashboard and Merge Request controls including security approvals.  This allows our users to have alternate security tools that either replace, or augment, our Secure tools.

As examples, GitLab will provide:
* A walkthrough (e.g., wizard) enabling users the ability to configure and enable integrations they wish to use as part of their DevSecOps processes.
* Standardize REST APIs, following CRUD principles, for integrating third-party security scanners the ability to start scanning via GitLab CI/CD as well as submit their security findings into the GitLab complete DevOps platform.
* Security vulnerability reporting framework, following JSON notation, allowing third-party security scanners to submit their security findings in such a way that their results can be integrated into Secure’s Security Dashboard and Merge Request controls including security approvals.

If you are interested in [contributing](https://docs.gitlab.com/ee/development/contributing/) such an integration, please [create an issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new?nav_source=navbar) so we can collaborate on questions and adding any enabling functionality.

## 3 Year Strategy

In three years the Secure Section market will:
* Have the application security testing (AST) market continue to grow, adding new techniques into its definition
    * This includes a bigger focus on API readiness with API fuzzing and security testing growing in popularity
    * See a resurgence of security testing within operations / production
* Continue the "Shift Left" trend, with Static Analysis (SAST) becoming more prominent
* Adopt machine learning techniques within the development phases of the DevOps lifecycle

As a result, in three years, Gitlab will:
* Identify insecure coding practices in real-time and provide recommendations to correct them prior to merge request
* Provide fuzz testing across the entire application technology stack (protocol, API, application), including both black-box and white-box testing techniques
* Expand Secure to support testing in development as well as in operations / production
    * Introduce categories that enable real-time and continuous verification of operations / production
* Expand focus within preexisting categories to apply knowledge / results from one scan type to optimize another scan type's configuration

## 1 Year Plan

### What's Next for Secure

To meet our [audacious goals](https://about.gitlab.com/company/strategy/#big-hairy-audacious-goal), the Secure Section will focus on the following over the next 12 months:
* **Dogfooding** - We will [“practice what we preach”](https://www.dictionary.com/browse/practice-what-you-preach), including leveraging Secure Categories in all things GitLab does.  This tight circle will provide immediate feedback and increase our rate of learning.
* **Security for everyone** - In order to make security accessible to everyone across the DevOps lifecycle, we will bring all Secure OSS scanners to Core (self-hosted) / Free (GitLab.com).
* **API first** - API proliferation will continue as more companies focus on ways to integrate technologies.  As such, we will focus on security testing of APIs including verifying standards like the [OWASP Top 10](https://www.owasp.org/index.php/Category:OWASP_Top_Ten_Project) and [API fuzz testing](https://gitlab.com/gitlab-org/gitlab/issues/33906).
* **Historical trending** - Provide a focus on identifying patterns in security findings with a goal of helping everyone code securely.  Make recommendations on remediation with a goal of providing automatic remediation wherever possible.
* **Provide Dynamic Analysis in production** - Enable Dynamic Analysis Categories to empower users to scan and assess applications and services deployed to production / operations.
* **Machine learning** - Machine learning (ML) will be leveraged, as part of static analysis, to identify insecure coding practices and help developers write more secure code.  This will be opt-in and will enable the power of the GitLab global community.
* **Integrations** - Enable third parties to easy integrate their security solutions into Ultimate (self-hosted) / Gold (GitLab.com). This includes providing APIs and a standard reporting framework so everyone can bring their preferred security tools into Ultimate (self-hosted) / Gold (GitLab.com).
* **Differentiate on value** - Running a security test is just the beginning.  We want to provide a first-class experience and enable users to make data-driven decisions to secure their applications and services as well as their enterprise.

### What We're Not Doing

The following will NOT be a focus over the next 12 months:
* **Protocol fuzzing** - Fuzzing the entire application technology stack is part of Secure’s 3 Year Strategy; however, we will focus on applications and APIs first.  The shift to protocol fuzzing will occur as Viable and Complete [maturities](https://about.gitlab.com/direction/maturity/#legend) are achieved on DAST and API security testing.
* **Security services** - The cybersecurity staffing shortage [continues to grow](https://www.forbes.com/sites/martenmickos/2019/06/19/the-cybersecurity-skills-gap-wont-be-solved-in-a-classroom/#79a380dd1c30) with no solvable solution yet defined.  To solve this issue, organizations have been relying on security services to fill this gap in their security processes.  As part of Secure’s 3 Year Strategy, we want to address this for the GitLab community by offering cybersecurity augmentation powered by GitLab Secure categories.
* **Responsible disclosure** - GitLab will become both a CVE Numbering Authority (CNA) for GitLab applications as well as for researchers and technologists to use when reporting new vulnerabilities (within any application, service, or operating system).  As part of Secure’s 3 Year Strategy, we want to support the entire vulnerability lifecycle to enable ease-of-use when it comes to [responsible disclosure](https://en.wikipedia.org/wiki/Responsible_disclosure).

Please explore the individual [Category](https://about.gitlab.com/direction/secure/#categories) Direction pages for more information on 12 month plans.

## Security Paradigm

We want to provide feedback during development and before your application is in production to reduce production vulnerabilities, and to reduce cycle time of releases by providing relevant point in time information. Think back on the recent breaches in the news, in most cases these were not the result of complex attacks, but rather teams who were unable for a variety of reasons to strictly follow best practices. We plan to assist you and your team to cover these, so your Security teams time and energy can focus on the more unique and advanced problems that you face in your risk profile, rather than spending time on things that are low hanging fruit.

The approach is to _support_ decision makers, not to replace them. Instead of
enforcing security, we want to give a very simple way to take the right action,
and learn from it. Keeping it simple is a key value to prevent security
features from being considered at all because they require more effort than the
perceived benefit.

Our tools will help provide security in depth for your application, will cover the most common vulnerabilities, and the strongly recommended security basics. The results and recommendations of our secure tools will be made available at the relevant time, as well as through a variety of methods such as an API so that the information can be used at the time and in the way that works best for your team. 

Tools are actionable: it means that users can
[interact with them and provide feedback](https://docs.gitlab.com/ee/user/project/merge_requests/#interacting-with-security-reports)
about their content. When triaging vulnerabilities, users can confirm
(creating an issue to solve the problem), or dismiss them (in case they are
false positives and there is no further action to take). This information will be
collected to improve the signal-to-noise ratio that the tool could provide in
future executions.

We want to allow your team the choices to set your risk level as is appropriate to your business. Even accurate results may be considered false positives as this is dependent on many details about both your application and your environment. By default our tools will never block a merge request or pipeline if they find vulnerabilities. However, GitLab wants to help you customize this behavior by setting up [Security approvals in Merge Requests](https://docs.gitlab.com/ee/user/application_security/#security-approvals-in-merge-requests-ultimate) and enforcing the execution of the analysis if this is what you need.

Our tools should be easy to use, and assist your team in making secure decisions with the minimal amount of effort and steps. Our findings should be informational enough to enable prioritizing and acting on findings in a way that works with your desired workflows and doesn’t result in users disabling or ignoring the checks,
preventing the benefit.

<%= partial("direction/categories", :locals => { :stageKey => "secure" }) %>

## Other top-level features

In the Secure stage there are features that are cross-category. They allow Security
Teams and Developers to manage security with a holistic approach.
The prioritization of a security issue doesn't fully depend on the type of
vulnerability, but on its severity and so which is the impact on the application.

That's why we want to create features where vulnerabilities are in one single place,
no matter if they are coming from SAST, Dependency Scanning, Container Scanning, etc.

### Auto Remediation

When a vulnerability is automatically detected by GitLab, users can be aware
of that using the Security Dashboard or looking at security reports. But this
still requires manual intervention to create a fix and push it to production,
and during this time there is a vulnerable window where attackers can leverage
the vulnerability.

Auto Remediation aims to automate vulnerability solution flow, and automatically create
a fix. The fix is then tested, and if it passes all the tests already defined
for the application, it is deployed to production.

GitLab can then monitor performances of the deployed app, and revert all the
changes in case performances are decreasing dramatically, warning the user
about the entire process and reducing the need for manual actions.

Read more in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/759).

<figure class="video_container">
   <iframe width="560" height="315" src="https://www.youtube.com/embed/ZgFqyXCsqPY?start=4216" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</figure>

Slides are available [here](https://docs.google.com/presentation/d/1VuUKRnMlISEK4ECPaJVOYv3Pom4Zh6JXBFbD58RuibI/edit#slide=id.g4286011fc8_8_0).

### Security Dashboards

Security Dashboards, available at [group](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
and [project](https://docs.gitlab.com/ee/user/project/security_dashboard.html) level,
are the primary tool for Security Teams and Directors of Security. They can use those
dashboards to access the current security status of their applications, and to start a
remediation process from here.

The dashboard is also giving stats and charts to figure out how the team is
performing to keep the security level to a proper level.

### Bill of materials

Software is often based on many components that are reused. Every modern programming
language makes it easy to pull and use external libraries via package managers.
There is a growing need to know exactly what is included in the final app, and the
relevant information about those third-party components, like the version number,
the license, and the security status.

The bill of materials (BOM) makes this information available and accessible, so
compliance can perform validation that the app can be released and deployed.

Read more in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/858).

## DevOps coverage

The classic DevOps includes security testing as an activity in the **Verify**
stage.

GitLab has a broader approach. That's why we introduced a **Secure** stage that
is transversal to all other **Dev** stages.

Our Product Vision aims to increase the security coverage for all these stages
in the [DevOps lifecycle](https://en.wikipedia.org/wiki/DevOps_toolchain):
**Plan**, **Create**, **Verify**, and **Package**.

This is possible because GitLab is a
[single application](https://about.gitlab.com/handbook/product/single-application/).

There are many advantages coming from complete coverage of all these stages with
security features. It allows to shift-left security at a very early stage, even
before the first lines of code are committed. If you start with security as a
priority when you plan a new app, it is easier than just trying to add security
later. Security should remain on the radar even when the create process happens,
to support developers and help them to write secure code from the beginning.
Security testing can then happen for any change, before it is merged into the
stable branch of the application: in this way you can spot problems and fix them
before they risk affecting the production environment or the stable version of
your app. Packaging the app should also be done with security in mind, as well
as the release process to the production environment: at that point, your code
is publicly available and any remediation will impact heavily on your processes.

Security is a never-ending priority, and at GitLab we want to make it easy for
our users to manage it.

## Target audience

### Security teams

We want to support security teams as first class citizens. GitLab should be
their primary tool to manage monitoring and remediation of security issues.
Using the **Security Dashboard** security specialists know exactly which is the
most important thing they need to take care of, while Directors of Security can
manage workflows and analyze historical data to figure out how to improve the
response time.

This is a vulnerability-centric approach where items are grouped and ordered to
suggest what's most important in a group, or in the entire instance.

### Developers

Nonetheless, we want to support developers and provide feedback during the
application development.
[**Security Reports**](https://docs.gitlab.com/ee/user/project/merge_requests/#security-reports)
in merge request widgets and pipelines allow early access to security
information that can be used to fix problems even before they are merged into
the stable branch or released to the public, embracing the idea of [shift left testing](https://en.wikipedia.org/wiki/Shift_left_testing).

This approach is valuable to highlight how specific changes could affect the
security of the application.

## Impact

We expect that in most of the cases the number of potential security issues will
be high, and we don't want that users will struggle in figuring out which is the
impact of any possible item for their environment. That's why GitLab will
prioritize security vulnerabilities based on different factors. The value given
with this process is defined as the impact.

These are examples of factors that may contribute to define the impact for a
specific security issue:
- severity and confidence levels, provided by the analysis tool
- feedback given in other reports for the same vulnerability
- exposure of the vulnerability (e.g., app has already been deployed to production)

## Availability

### Ultimate/Gold subscribers

For now, security features are available in the [Ultimate/Gold tier](https://about.gitlab.com/pricing/)
because we think that security matters for everyone, but automation of security
in the development lifecycle is more valuable for organizations who want to
optimize their DevOps investment to run securely at the highest business
velocity.

### Public projects on GitLab.com

Every project on [GitLab.com](https://gitlab.com) with
[public visibility](https://docs.gitlab.com/ee/public_access/public_access.html#public-projects)
can benefit of all the security features for free, even if it doesn't have a Gold license.

Nevertheless, public groups can benefit of group security features (like the
[Group Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
only if they have a Gold license.

## Metrics

We also want to collect and provide metrics to better understand how the
security workflow is performing. For example, the overall time taken to deploy a
fix for a vulnerability once spotted could be useful for flow improvements.

## Prioritization Process

Every month, a couple of weeks before the start of the development cycle, the
team meets to plan what will be done in the upcoming milestone. Decisions are
based on our general
[prioritization principles](https://about.gitlab.com/handbook/product/#prioritization),
and we specifically consider case by case what is most important to do next,
since we always have so many awesome ideas but we have to choose what has to be
done first.

The PM maintains a [product roadmap](https://gitlab.com/groups/gitlab-org/-/boards/767789)
for the next three milestones and beyond, to ensure that we are focusing on
important things ahead of time and we can prepare them properly. We are always
ready to adjust our plans and we normally commit only to issues that are
scheduled for the current release: this is very important in order to guarantee
that we can be fast enough to react to unexpected events.

We want everybody to be part of our prioritization process, that's why all the
information is public. We encourage team members, customer, and the community to
give feedback and to share what they consider very important to focus on in the
future. This is a valuable help to make GitLab a great product.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "secure" }) %>
