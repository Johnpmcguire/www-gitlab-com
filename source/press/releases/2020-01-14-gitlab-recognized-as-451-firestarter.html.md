---
layout: markdown_page
title: "GitLab Recognized by 451 Research as a ‘451 Firestarter’"
---

_The complete DevOps platform delivered as a single application recognized by leading analyst firm for innovation and vision in the technology industry_

**SAN FRANCISCO, CALIFORNIA - January 14, 2019 —** [GitLab Commit](https://about.gitlab.com/events/commit/#schedule) —January 14, 2020 — [GitLab](https://about.gitlab.com/), the DevOps platform delivered as a single application, today announced it has received a 451 Firestarter award from leading technology research and advisory firm [451 Research](https://451research.com/), recognizing the company’s innovative contribution within the technology industry.  

451 Research’s Firestarter program recognizes exceptional innovation within the information technology industry. Introduced in 2018, and awarded quarterly, the program is exclusively analyst-led, allowing its team of technology and market experts to highlight organizations they believe are significantly contributing to the overall pace and extent of innovation in the technology market. 

GitLab was recognized for gaining traction in the enterprise as DevOps continues to drive IT spending for digital transformation. Its growing breadth of coverage of enterprise CI/CD releases, and its destination as a single home for DevOps tools and components, make it appealing to enterprise teams wary of tool sprawl and complexity. The company is also unique in its priority on security, monitoring, and code analytics, which put it at the forefront of trends within DevOps, including DevSecOps and DataOps. 

“We’re honored to be recognized as a 451 Firestarter this quarter,” said Sid Sijbrandij, CEO and co-founder of GitLab. “Like the name of the award describes, GitLab’s growth accelerated at the pace of a blazing fire in Q4, and we will continue this momentum while remaining committed to our goal of allowing everyone to contribute while helping organizations deliver innovative software to market securely and quickly, but also remaining true to our roots as a fully open source, transparent company.”

“451 Research has built its reputation on helping organizations understand innovation and disruption in the enterprise IT industry, and the Firestarter award is one of the ways we spotlight important trends and players,'’ said Jay Lyman, 451 Research Principal Analyst. '’Innovative approaches from companies such as GitLab — with its open source software technology and community contributions as well as transparency with customers — merit the recognition of a 451 Firestarter award.”


#### About GitLab
GitLab is a DevOps platform built from the ground up as a single application for all stages of the DevOps lifecycle enabling Product, Development, QA, Security, and Operations teams to work concurrently on the same project. GitLab provides teams a single data store, one user interface, and one permission model across the DevOps lifecycle, allowing teams to collaborate and work on a project from a single conversation, significantly reducing cycle time and focus exclusively on building great software quickly. Built on open source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations. More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network, and Comcast trust GitLab to deliver great software at new speeds.

#### About 451 Research
451 Research is a leading information technology research and advisory company focusing on technology innovation and market disruption. More than 100 analysts and consultants provide essential insight to more than 2,000 client organizations globally through syndicated research, advisory services and live events. Founded in 2000 and headquartered in New York, 451 Research is a division of the 451 Group. [Learn more](https://451research.com/about-us/our-research/451-firestarters) about the 451 Research Firestarters.

#### Media Contact
Natasha Woods
<br>
GitLab
<br>
press@gitlab.com
